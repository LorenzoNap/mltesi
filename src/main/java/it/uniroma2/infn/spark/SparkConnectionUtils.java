package it.uniroma2.infn.spark;

import it.uniroma2.infn.utils.PropertiesUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * Created by Lorenzo on 29/02/16.
 */
public class SparkConnectionUtils {

    private static SparkConnectionUtils instance = null;

    private SparkConf sparkConf;
    private JavaSparkContext javaSparkContext;

    protected SparkConnectionUtils() {
        //Inizializza il context di spark
        sparkConf = new SparkConf().setAppName("TESI_ML").setMaster(PropertiesUtils.readValues(PropertiesUtils.SPARK_MASTER_URI_KEY)).set("spark.driver.allowMultipleContexts","true");
        javaSparkContext = new JavaSparkContext(sparkConf);
    }
    public static SparkConnectionUtils getInstance() {
        if(instance == null) {
            instance = new SparkConnectionUtils();
        }
        return instance;
    }

    public static void setInstance(SparkConnectionUtils instance) {
        SparkConnectionUtils.instance = instance;
    }

    public SparkConf getSparkConf() {
        return sparkConf;
    }

    public void setSparkConf(SparkConf sparkConf) {
        this.sparkConf = sparkConf;
    }

    public JavaSparkContext getJavaSparkContext() {
        return javaSparkContext;
    }

    public void setJavaSparkContext(JavaSparkContext javaSparkContext) {
        this.javaSparkContext = javaSparkContext;
    }
}
