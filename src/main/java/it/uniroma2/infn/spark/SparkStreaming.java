package it.uniroma2.infn.spark;

import it.uniroma2.infn.utils.PropertiesUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionTrainingSummary;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.*;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.Time;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import scala.Tuple2;
import org.apache.spark.sql.functions.*;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by Lorenzo on 29/02/16.
 */
public class SparkStreaming implements Serializable{


    public SparkStreaming() {

//        SparkConf conf = new SparkConf().setAppName("TESI_ML").setMaster(PropertiesUtils.readValues(PropertiesUtils.SPARK_MASTER_URI_KEY));
        SparkConnectionUtils sparkConnectionUtils =SparkConnectionUtils.getInstance();

//        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConnectionUtils.getSparkConf());

        JavaStreamingContext jssc = new JavaStreamingContext(sparkConnectionUtils.getSparkConf(), Durations.seconds(5));
//        //SQL context
//        SQLContext sqlContext = new org.apache.spark.sql.SQLContext(javaSparkContext);


        JavaReceiverInputDStream<String> lines = jssc.socketTextStream(PropertiesUtils.readValues(PropertiesUtils.STREAMING_IP), Integer.parseInt(PropertiesUtils.readValues(PropertiesUtils.STREAMING_PORT)));

        JavaDStream<String> parsedLines = lines.flatMap(
                new FlatMapFunction<String, String>() {
                    public Iterable<String> call(String x) {
                        return Arrays.asList(x.split("\n"));
                    }
                });


        parsedLines.foreachRDD(
                new Function2<JavaRDD<String>, Time, Void>() {
                    public Void call(JavaRDD<String> rdd, Time time) {

                        if(!rdd.isEmpty()) {
                            // Get the singleton instance of SQLContext
                            SQLContext sqlContext = SQLContext.getOrCreate(rdd.context());
                            DataFrame dataFrame = sqlContext.jsonRDD(rdd);

                            dataFrame.collect();
                            dataFrame.printSchema();
                            dataFrame.select(org.apache.spark.sql.functions.concat_ws(",",new  Column[]{dataFrame.col("alarm_BTF/CHHTB002_o"), dataFrame.col("alarm_BTF/CVVTB001_o")})).collect();

//                            VectorAssembler assembler = new VectorAssembler().setInputCols(new String[]{"alarm_BTF/CHHTB002_o", "alarm_BTF/CVVTB001_o", "alarm_BTF/CVVTB002_o", "alarm_BTF/DHSTB001_o", "alarm_BTF/DHSTB002_o", "alarm_BTF/QUATB001_o", "alarm_BTF/QUATB002_o", "alarm_BTF/QUATB003_o", "alarm_BTF/QUATB004_o", "alarm_BTF/QUATB101_o", "alarm_BTF/QUATB102_o", "alarms_BTF/CHHTB002_o", "alarms_BTF/CVVTB001_o", "alarms_BTF/CVVTB002_o", "alarms_BTF/DHSTB001_o", "alarms_BTF/DHSTB002_o", "alarms_BTF/QUATB001_o", "alarms_BTF/QUATB002_o", "alarms_BTF/QUATB003_o", "alarms_BTF/QUATB004_o", "alarms_BTF/QUATB101_o", "alarms_BTF/QUATB102_o", "current_BTF/CHHTB002_o", "current_BTF/CVVTB001_o", "current_BTF/CVVTB002_o", "current_BTF/DHSTB001_o", "current_BTF/DHSTB002_o", "current_BTF/QUATB001_o", "current_BTF/QUATB002_o", "current_BTF/QUATB003_o", "current_BTF/QUATB004_o", "current_BTF/QUATB101_o", "current_BTF/QUATB102_o", "current_sp_BTF/CHHTB002_o", "current_sp_BTF/CVVTB001_o", "current_sp_BTF/CVVTB002_o", "current_sp_BTF/DHSTB001_o", "current_sp_BTF/DHSTB002_o", "current_sp_BTF/QUATB001_o", "current_sp_BTF/QUATB002_o", "current_sp_BTF/QUATB003_o", "current_sp_BTF/QUATB004_o", "current_sp_BTF/QUATB101_o", "current_sp_BTF/QUATB102_o", "dev_state_BTF/CHHTB002_o", "dev_state_BTF/CVVTB001_o", "dev_state_BTF/CVVTB002_o", "dev_state_BTF/DHSTB001_o", "dev_state_BTF/DHSTB002_o", "dev_state_BTF/QUATB001_o", "dev_state_BTF/QUATB002_o", "dev_state_BTF/QUATB003_o", "dev_state_BTF/QUATB004_o", "dev_state_BTF/QUATB101_o", "dev_state_BTF/QUATB102_o", "dpck_ats_BTF/CHHTB002_o", "dpck_ats_BTF/CVVTB001_o", "dpck_ats_BTF/CVVTB002_o", "dpck_ats_BTF/DHSTB001_o", "dpck_ats_BTF/DHSTB002_o", "dpck_ats_BTF/QUATB001_o", "dpck_ats_BTF/QUATB002_o", "dpck_ats_BTF/QUATB003_o", "dpck_ats_BTF/QUATB004_o", "dpck_ats_BTF/QUATB101_o", "dpck_ats_BTF/QUATB102_o", "dpck_ds_type_BTF/CHHTB002_o", "dpck_ds_type_BTF/CVVTB001_o", "dpck_ds_type_BTF/CVVTB002_o", "dpck_ds_type_BTF/DHSTB001_o", "dpck_ds_type_BTF/DHSTB002_o", "dpck_ds_type_BTF/QUATB001_o", "dpck_ds_type_BTF/QUATB002_o", "dpck_ds_type_BTF/QUATB003_o", "dpck_ds_type_BTF/QUATB004_o", "dpck_ds_type_BTF/QUATB101_o", "dpck_ds_type_BTF/QUATB102_o", "on_BTF/CHHTB002_o", "on_BTF/CVVTB001_o", "on_BTF/CVVTB002_o", "on_BTF/DHSTB001_o", "on_BTF/DHSTB002_o", "on_BTF/QUATB001_o", "on_BTF/QUATB002_o", "on_BTF/QUATB003_o", "on_BTF/QUATB004_o", "on_BTF/QUATB101_o", "on_BTF/QUATB102_o", "polarity_BTF/CHHTB002_o", "polarity_BTF/CVVTB001_o", "polarity_BTF/CVVTB002_o", "polarity_BTF/DHSTB001_o", "polarity_BTF/DHSTB002_o", "polarity_BTF/QUATB001_o", "polarity_BTF/QUATB002_o", "polarity_BTF/QUATB003_o", "polarity_BTF/QUATB004_o", "polarity_BTF/QUATB101_o", "polarity_BTF/QUATB102_o", "stby_BTF/CHHTB002_o", "stby_BTF/CVVTB001_o", "stby_BTF/CVVTB002_o", "stby_BTF/DHSTB001_o", "stby_BTF/DHSTB002_o", "stby_BTF/QUATB001_o", "stby_BTF/QUATB002_o", "stby_BTF/QUATB003_o", "stby_BTF/QUATB004_o", "stby_BTF/QUATB101_o", "stby_BTF/QUATB102_o", "voltage_BTF/CHHTB002_o", "voltage_BTF/CVVTB001_o", "voltage_BTF/CVVTB002_o", "voltage_BTF/DHSTB001_o", "voltage_BTF/DHSTB002_o", "voltage_BTF/QUATB001_o", "voltage_BTF/QUATB002_o", "voltage_BTF/QUATB003_o", "voltage_BTF/QUATB004_o", "voltage_BTF/QUATB101_o", "voltage_BTF/QUATB102_o"})
//                                    .setOutputCol("features");
                            VectorAssembler assembler = new VectorAssembler().setInputCols(new String[]{"alarm_BTF/CHHTB002_o", "alarm_BTF/CVVTB001_o", })
                                    .setOutputCol("features");
                            dataFrame = assembler.transform(dataFrame);

                            dataFrame = dataFrame.selectExpr("features as features", "tag as label");
//                        dataFrame = dataFrame.select("features", dataFrame.label.cast("double"));
                            dataFrame.select("features", "label").show();


                            JavaRDD<LabeledPoint> parsedData = dataFrame.javaRDD().map(line -> {
                                int indexLabel = (line.fieldIndex("label"));
                                int indexFeatures = (line.fieldIndex("features"));
                                return new LabeledPoint(line.getDouble(indexLabel), Vectors.dense(indexFeatures));
                            });


                            parsedData.cache();
                            parsedData.collect();


//                            // Building the model
//                            int numIterations = 100;
//                            final LinearRegressionModel model =
//                                    LinearRegressionWithSGD.train(JavaRDD.toRDD(parsedData), numIterations);
//
//                            // Evaluate model on training examples and compute training error
//                            JavaRDD<Tuple2<Double, Double>> valuesAndPreds = parsedData.map(
//                                    new Function<LabeledPoint, Tuple2<Double, Double>>() {
//                                        public Tuple2<Double, Double> call(LabeledPoint point) {
//                                            double prediction = model.predict(point.features());
//                                            return new Tuple2<Double, Double>(prediction, point.label());
//                                        }
//                                    }
//                            );
//                            System.out.println(valuesAndPreds.collect());
//                            double MSE = new JavaDoubleRDD(valuesAndPreds.map(
//                                    new Function<Tuple2<Double, Double>, Object>() {
//                                        public Object call(Tuple2<Double, Double> pair) {
//                                            return Math.pow(pair._1() - pair._2(), 2.0);
//                                        }
//                                    }
//                            ).rdd()).mean();
//                            System.out.println("training Mean Squared Error = " + MSE);




                            //Building the model
                            final MyStreamingLinearRegressionWithSGD streamingLinearRegressionWithSGD = new MyStreamingLinearRegressionWithSGD();
                            streamingLinearRegressionWithSGD.setInitialWeights(Vectors.zeros(assembler.getInputCols().length +2));


                            streamingLinearRegressionWithSGD.trainOn(JavaRDD.toRDD(parsedData));


                            JavaRDD<Tuple2<Double, Double>> valuesAndPreds2 = parsedData.map(
                                    new Function<LabeledPoint, Tuple2<Double, Double>>() {
                                        public Tuple2<Double, Double> call(LabeledPoint point) {
                                            double prediction = streamingLinearRegressionWithSGD.predict(point.features());
                                            return new Tuple2<Double, Double>(prediction, point.label());
                                        }
                                    });
                            System.out.println("AHAHAHAHA" + valuesAndPreds2.collect());

//                        streamingLinearRegressionWithSGD.predictOnValues(JavaRDD.toRDD(parsedData).map(lp -> {
//                            lp.label(),lp.features();
//                        }));

//                        streamingLinearRegressionWithSGD.predictOnValues(testData.map(lp => (lp.label, lp.features))).print()
//                        LinearRegression lr = new LinearRegression()
//                                .setMaxIter(10)
//                                .setRegParam(0.3)
//                                .setElasticNetParam(0.8);
//
//                        // Fit the model
//                        org.apache.spark.ml.regression.LinearRegressionModel lrModel = lr.fit(dataFrame);
//                        // Print the coefficients and intercept for linear regression
//                        System.out.println("Coefficients: "
//                                + lrModel.coefficients() + " Intercept: " + lrModel.intercept());
//
//                        LinearRegressionTrainingSummary trainingSummary = lrModel.summary();
//                        System.out.println("numIterations: " + trainingSummary.totalIterations());
//                        System.out.println("objectiveHistory: " + Vectors.dense(trainingSummary.objectiveHistory()));
//                        trainingSummary.residuals().show();
//                        System.out.println("RMSE: " + trainingSummary.rootMeanSquaredError());
//                        System.out.println("r2: " + trainingSummary.r2());

                            //Transform row to labeld point


                        }
                        return null;

                    }
                }
        );



        parsedLines.print();

        jssc.start();              // Start the computation
        jssc.awaitTermination();   // Wait for the computati


    }
}
