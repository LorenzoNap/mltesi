package it.uniroma2.infn.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Lorenzo on 29/02/16.
 */
public class PropertiesUtils {

    public static final String SPARK_MASTER_URI_KEY = "spark_master_uri_key";
    public static final String STREAMING_IP = "streaming_ip";
    public static final String STREAMING_PORT = "streaming_port";

    public static String readValues(String key){
        Properties prop = new Properties();
        InputStream input = null;
        String value = "";
        try {

            input = PropertiesUtils.class.getClassLoader().getResourceAsStream("config.properties");

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            value = prop.getProperty(key);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return value;
        }
    }
}
