package it.uniroma2.infn;

import it.uniroma2.infn.spark.SparkConnectionUtils;
import it.uniroma2.infn.spark.SparkStreaming;
import it.uniroma2.infn.utils.PropertiesUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.launcher.SparkAppHandle;
import org.apache.spark.launcher.SparkLauncher;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class App
{

    private static Logger logger = Logger.getLogger(App.class.getCanonicalName());


    public static void main( String[] args ) throws IOException {

        logger.info("Inizializzo il context di spark");
        SparkStreaming sparkStreaming = new SparkStreaming();
        SparkConnectionUtils.getInstance().getJavaSparkContext().close();
    }
}
